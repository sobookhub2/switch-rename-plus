// ---------Helper--------- //
function debugLog(s, message) {
	const debugLog = s.getPropertyValue("debugLog") === "Yes";
	if (debugLog) {
		s.log(2, message);
	}
}

function parseProperFileNameAndExtension(s, str, file, actOn) {
	const newFile = {
		properName: actOn === "Complete filename" ? "" : str,
		extension: actOn === "Complete Filename" ? "" : file.extension
	};
	if (actOn === "Complete filename") {
		const matches = str.split(".");
		for (var i = 0; i < matches.length; i++) {
			if (i + 1 !== matches.length) {
				newFile.properName += matches[i];
				if (i + 2 !== matches.length) {
					newFile.properName += ".";
				}
			} else {
				if (!newFile.properName) {
					newFile.properName = matches[i];
				} else {
					newFile.extension = matches[i];
				}
			}
		}
	}
	return newFile;
}

function generateSegment(startIndex, endIndex, str) {
	const segment = {
		start: parseInt(startIndex) - 1,
		end: !endIndex ? str.length : parseInt(endIndex) - 1
	};
	if (segment.end > str.length) {
		segment.end = str.length;
	}
	return segment.end >= segment.start ? segment : null;
}

function mergeFileProperties(s, file, actOn) {
	return actOn === "Complete filename" && file.extension ? file.properName + "." + file.extension : file.properName;
}

function escapeRegexForbiddenCharacter(str) {
	var escapedStr = "";
	const regexForbiddenCharacters = [".", "*", "+", "?", "^", "$", "{", "}", "(", ")", "|", "[", "]", "\\"];

	for (var i = 0; i < str.length; i++) {
		for (j = 0; j < regexForbiddenCharacters.length; j++) {
			if (regexForbiddenCharacters[j] === str.charAt(i)) {
				escapedStr = escapedStr + "\\";
				break;
			}
		}
		escapedStr += str.charAt(i);
	}
	return escapedStr;
}

function searchAndReplace(s, str, search, replace, repeat) {
	search = escapeRegexForbiddenCharacter(search);
	const flag = repeat !== "Once" ? "g" : "";
	if (repeat !== "Recursive") {
		return str.replace(new RegExp(search, flag), replace);
	} else {
		for (var i = 0; i < 99; i++) {
			//max = 99 in order to avoid infinite loop
			return str.replace(new RegExp(search, flag), replace);
		}
	}
}

function checkSearchAndReplaceArrayValue(s, value) {
	value = value.split("\n");
	for (i = 0; i < value.length; i++) {
		if (!value[i].match(/^[^=]+=[^=]*$/)) {
			return false;
		}
	}
	return true;
}

function removeDiacriticsFromString(s, str) {
	const letterAccents = [
		{ letter: "a", accents: "[áăắặằẳẵǎâấậầẩẫäạàảāąåǻãɑɐɒ]", smallLetterOnly: false },
		{ letter: "ae", accents: "[æǽ]", smallLetterOnly: false },
		{ letter: "b", accents: "[ḅɓ]", smallLetterOnly: false },
		{ letter: "c", accents: "[ćčçĉɕċ]", smallLetterOnly: false },
		{ letter: "d", accents: "[ďḓḍɗḏđɖʤǳʣʥǆð]", smallLetterOnly: false },
		{ letter: "e", accents: "[éĕêěếệềểễëėẹèẻēęẽʒǯʓɘɜɝəɚʚɞ]", smallLetterOnly: false },
		{ letter: "f", accents: "[ƒʩ]", smallLetterOnly: false },
		{ letter: "f", accents: "[ﬁﬂ]", smallLetterOnly: true },
		{ letter: "g", accents: "[ǵğǧģĝġɠḡɡɣ]", smallLetterOnly: false },
		{ letter: "h", accents: "[ḫĥḥɦẖħɧɥʮʯ]", smallLetterOnly: false },
		{ letter: "i", accents: "[íĭǐîïịìỉīįɨĩɩıĳɟ]", smallLetterOnly: false },
		{ letter: "j", accents: "[ǰĵʝȷɟʄ]", smallLetterOnly: false },
		{ letter: "k", accents: "[ķḳƙḵĸʞ]", smallLetterOnly: false },
		{ letter: "l", accents: "[ĺƚɬľļḽḷḹḻŀɫɭłƛɮǉʪʫ]", smallLetterOnly: false },
		{ letter: "m", accents: "[ḿṁṃɱɯɰ]", smallLetterOnly: false },
		{ letter: "n", accents: "[ŉńňņṋṅṇǹɲṉɳñǌŋŊ]", smallLetterOnly: false },
		{ letter: "o", accents: "[óŏǒôốộồổỗöọőòỏơớợờởỡōǫøǿõɛɔɵʘ]", smallLetterOnly: false },
		{ letter: "oe", accents: "[œ]", smallLetterOnly: false },
		{ letter: "p", accents: "[ɸþ]", smallLetterOnly: false },
		{ letter: "q", accents: "[ʠ]", smallLetterOnly: false },
		{ letter: "r", accents: "[ŕřŗṙṛṝɾṟɼɽɿɹɻɺ]", smallLetterOnly: false },
		{ letter: "s", accents: "[śšşŝșṡṣʂſʃʆʅ]", smallLetterOnly: false },
		{ letter: "ss", accents: "[ß]", smallLetterOnly: true },
		{ letter: "t", accents: "[ťţṱțẗṭṯʈŧʨʧʦʇ]", smallLetterOnly: false },
		{ letter: "u", accents: "[ʉúŭǔûüǘǚǜǖụűùủưứựừửữūųůũʊ]", smallLetterOnly: false },
		{ letter: "v", accents: "[ʋʌ]", smallLetterOnly: false },
		{ letter: "w", accents: "[ẃŵẅẁʍʬ]", smallLetterOnly: false },
		{ letter: "y", accents: "[ýŷÿẏỵỳƴỷȳỹʎ]", smallLetterOnly: false },
		{ letter: "z", accents: "[źžʑżẓẕʐƶ]", smallLetterOnly: false }
	];
	for (var i = 0; i < letterAccents.length; i++) {
		str = str.replace(new RegExp(letterAccents[i].accents, "g"), letterAccents[i].letter);
		if (!letterAccents[i].smallLetterOnly) {
			str = str.replace(new RegExp(letterAccents[i].accents.toUpperCase(), "g"), letterAccents[i].letter.toUpperCase());
		}
	}
	return str;
}

function isUpperCase(character) {
	return character.match(/[A-Z]/);
}

function isLowerCase(character) {
	return character.match(/[a-z]/);
}

function isNumber(character) {
	return character.match(/[0-9]/);
}

function packWord(words, word) {
	words.push(word.value);
	word.value = "";
	word.type = undefined;
}

function findWordsManually(s, str) {
	str = removeDiacriticsFromString(s, str);
	const words = [];
	const word = {
		value: "",
		type: undefined
	};
	for (var i = 0; i < str.length; i++) {
		if (isUpperCase(str.charAt(i))) {
			if (!word.type || word.type === "upperCase") {
				word.type = "upperCase";
				word.value += str.charAt(i);
			} else {
				packWord(words, word);
				i--;
			}
		} else if (isLowerCase(str.charAt(i))) {
			if (!word.type || word.type === "lowerCase" || (word.type === "upperCase" && word.value.length === 1)) {
				word.type = "lowerCase";
				word.value += str.charAt(i);
			} else {
				packWord(words, word);
				i--;
			}
		} else if (isNumber(str.charAt(i))) {
			if (!word.type || word.type === "number") {
				word.type = "number";
				word.value += str.charAt(i);
			} else {
				packWord(words, word);
				i--;
			}
		} else if (word.value) {
			packWord(words, word);
		}
	}
	if (word.value) {
		packWord(words, word);
	}
	return words;
}

function findWordsWithSplit(s, str, delimitor) {
	str = str.split(delimitor);
	return str;
}

function findWords(s, str, delimitor) {
	return delimitor === "Automatic" ? findWordsManually(s, str) : findWordsWithSplit(s, str, delimitor);
}

function toPascalCase(s, str, delimitor) {
	const words = findWords(s, str, delimitor);
	for (var i = 0; i < words.length; i++) {
		words[i] = words[i].toLowerCase();
		words[i] = words[i].charAt(0).toUpperCase() + words[i].substring(1);
	}
	return words.join("");
}

function toCamelCase(s, str, delimitor) {
	const words = findWords(s, str, delimitor);
	for (var i = 0; i < words.length; i++) {
		words[i] = words[i].toLowerCase();
		if (i !== 0) {
			words[i] = words[i].charAt(0).toUpperCase() + words[i].substring(1);
		}
	}
	return words.join("");
}

function toKebabCase(s, str, delimitor) {
	const words = findWords(s, str, delimitor);
	return words.join("-").toLowerCase();
}

function toSnakeCase(s, str, delimitor) {
	const words = findWords(s, str, delimitor);
	return words.join("_").toLowerCase();
}

// ---------RenameFunctions--------- //
function caseConvention(s, file, actionNumber) {
	const caseConventionChoice = s.getPropertyValue("action_" + actionNumber + "_case_convention_choice");
	const caseConventionDelimitor = s.getPropertyValue("action_" + actionNumber + "_case_convention_delimitor");
	const caseConventionMethods = {
		snake_case: toSnakeCase,
		camelCase: toCamelCase,
		PascalCase: toPascalCase,
		"kebab-case": toKebabCase
	};
	file.properName = caseConventionMethods[caseConventionChoice](s, file.properName, caseConventionDelimitor);
	return file;
}

function replaceExtension(s, file, actionNumber) {
	file.extension = s.getPropertyValue("action_" + actionNumber + "_replace_extension_value");
	return file;
}

function addExtension(s, file, actionNumber) {
	const newExtension = s.getPropertyValue("action_" + actionNumber + "_add_extension_value");
	if (file.extension) {
		file.properName += "." + file.extension;
	}
	file.extension = newExtension;
	return file;
}

function deleteExtension(s, file) {
	file.extension = "";
	return file;
}

function trim(s, file, actionNumber) {
	file.properName.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
	return file;
}

function removeExtraSpaces(s, file, actionNumber) {
	const removeExtraSpacesActOn = s.getPropertyValue("action_" + actionNumber + "_extra_spaces_act_on");
	file.properName = file.properName.replace(/\s\s+/g, " ");
	if (removeExtraSpacesActOn === "Complete filename") {
		file.extension = file.extension.replace(/\s\s+/g, " ");
	}
	return file;
}

function removeDiacritics(s, file, actionNumber) {
	file.properName = removeDiacriticsFromString(s, file.properName);
	return file;
}

function removeForbiddenUNIXFileNameCharacters(s, file, actionNumber) {
	file.properName.replace(/[\/\\]/g, "");
	file.extension.replace(/[\/\\]/g, "");
	return file;
}

function removeForbiddenWindowsFileNameCharacters(s, file, actionNumber) {
	file.properName.replace(/[<>:"\/\\|?*]/g, "");
	file.extension.replace(/[<>:"\/\\|?*]/g, "");
	return file;
}

function searchAndReplaceArray(s, file, actionNumber) {
	const actOn = s.getPropertyValue("action_" + actionNumber + "_search_and_replace_array_act_on");
	const repeat = s.getPropertyValue("action_" + actionNumber + "_search_and_replace_array_repeat_type");
	var str = mergeFileProperties(s, file, actOn);

	var searchAndReplaces = s.getPropertyValue("action_" + actionNumber + "_search_and_replace_array_value");
	searchAndReplaces = searchAndReplaces.split("\n");

	for (var i = 0; i < searchAndReplaces.length; i++) {
		const searchAndReplaceArray = searchAndReplaces[i].split("=");
		str = searchAndReplace(s, str, searchAndReplaceArray[0], searchAndReplaceArray[1], repeat);
	}
	return parseProperFileNameAndExtension(s, str, file, actOn);
}

function searchAndReplaceString(s, file, actionNumber) {
	const actOn = s.getPropertyValue("action_" + actionNumber + "_search_and_replace_act_on");
	var search = s.getPropertyValue("action_" + actionNumber + "_search_and_replace_search_value");
	const replace = s.getPropertyValue("action_" + actionNumber + "_search_and_replace_replace_value");
	const repeat = s.getPropertyValue("action_" + actionNumber + "_search_and_replace_repeat_type");
	var str = mergeFileProperties(s, file, actOn);
	str = searchAndReplace(s, str, search, replace, repeat);
	return parseProperFileNameAndExtension(s, str, file, actOn);
}

function replace(s, file, actionNumber) {
	const actOn = s.getPropertyValue("action_" + actionNumber + "_replace_act_on");
	const replace = s.getPropertyValue("action_" + actionNumber + "_replace_replace_by");
	if (actOn === "Filename proper") {
		file.properName = replace;
	} else {
		file = parseProperFileNameAndExtension(s, replace, file, actOn);
	}
	return file;
}

function keepSegment(s, file, actionNumber) {
	const actOn = s.getPropertyValue("action_" + actionNumber + "_keep_segment_act_on");
	var segment = s.getPropertyValue("action_" + actionNumber + "_keep_segment_value");
	const str = mergeFileProperties(s, file, actOn);
	segment = generateSegment(startIndex, endIndex, str);
	if (segment === null) {
		return file;
	}
	var newStr = "";
	for (var i = 0; i < str.length; i++) {
		if (i >= segment.start && i <= segment.end) {
			newStr += str.charAt(i);
		}
	}
	return parseProperFileNameAndExtension(s, newStr, file, actOn);
}

function removeSegment(s, file, actionNumber) {
	const actOn = s.getPropertyValue("action_" + actionNumber + "_remove_segment_act_on");
	const startIndex = s.getPropertyValue("action_" + actionNumber + "_remove_segment_start_index");
	const endIndex = s.getPropertyValue("action_" + actionNumber + "_remove_segment_end_index");
	const str = mergeFileProperties(s, file, actOn);
	segment = generateSegment(startIndex, endIndex, str);
	if (segment === null) {
		return file;
	}
	var newStr = "";
	for (var i = 0; i < str.length; i++) {
		if (i < segment.start || i > segment.end) {
			newStr += str.charAt(i);
		}
	}
	return parseProperFileNameAndExtension(s, newStr, file, actOn);
}

function addSuffix(s, file, actionNumber) {
	const actOn = s.getPropertyValue("action_" + actionNumber + "_add_suffix_act_on");
	const suffix = s.getPropertyValue("action_" + actionNumber + "_add_suffix_value");
	if (actOn === "Complete filename" && file.extension) {
		file.extension += suffix;
	} else {
		file.properName += suffix;
	}
	return file;
}

function addPrefix(s, file, actionNumber) {
	const prefix = s.getPropertyValue("action_" + actionNumber + "_add_prefix_value");
	file.properName = prefix + file.properName;
	return file;
}

// ---------secondaryFunctions--------- //
function writeOldNameInPrivateData(s, job) {
	if (s.getPropertyValue("rememberOriginalName") === "Yes") {
		job.setPrivateData(s.getPropertyValue("privateDataKey"), job.getName());
		debugLog(s, 'Old Name "' + job.getName() + '" saved in privateDataKey "' + s.getPropertyValue("privateDataKey") + '"');
	}
}

function isJobAllowed(s, job) {
	const affectJobNameFor = s.getPropertyValue("affectJobNameFor");
	return affectJobNameFor === "All jobs" || (affectJobNameFor === "Individual files only" && job.isFile()) || (affectJobNameFor === "Job folders only" && job.isFolder());
}

// --------- MainFunction --------- //
function isPropertyValid(s: Switch, tag: String, value: String) {
	const tagConditions = [
		{ tagSuffix: "remove_segment_start_index", condition: !(!value || value === "0") },
		{ tagSuffix: "remove_segment_end_index", condition: value === "" || (value.match(/^[0-9]+$/) && parseInt(value) !== 0) },
		{ tagSuffix: "keep_segment_start_index", condition: !(!value || value === "0") },
		{ tagSuffix: "keep_segment_end_index", condition: value === "" || (value.match(/^[0-9]+$/) && parseInt(value) !== 0) },
		{ tagSuffix: "search_and_replace_search_value", condition: !!value },
		{ tagSuffix: "search_and_replace_array_value", condition: checkSearchAndReplaceArrayValue(s, value) },
		{ tagSuffix: "case_convention_delimitor", condition: !!value }
	];
	const tagActionIndexesConditions = {};
	const maxAction = 5; //The number max of action, actually one, target 10
	for (var i = 0; i < tagConditions.length; i++) {
		for (var j = 1; j <= maxAction; j++) {
			tagActionIndexesConditions["action_" + j + "_" + tagConditions[i].tagSuffix] = tagConditions[i].condition;
		}
	}
	return tagActionIndexesConditions[tag];
}

function jobArrived(s: Switch, job: Job) {
	if (!isJobAllowed(s, job)) {
		s.log(2, "Job is not allowed");
		job.sendToSingle(job.getPath(), job.getName());
	} else {
		writeOldNameInPrivateData(s, job);
		var file = {
			properName: job.getNameProper(),
			extension: job.getExtension()
		};

		const actionMethods = {
			None: function (s, file) {
				return file;
			},
			"Add prefix": addPrefix,
			"Add suffix": addSuffix,
			"Remove segment": removeSegment,
			"Keep segment": keepSegment,
			Replace: replace,
			"Search and replace": searchAndReplaceString,
			"Search and replace list": searchAndReplaceArray,
			"Remove forbidden Windows filename characters": removeForbiddenWindowsFileNameCharacters,
			"Remove forbidden UNIX filename characters": removeForbiddenUNIXFileNameCharacters,
			"Remove diacritics": removeDiacritics,
			"Remove extra spaces": removeExtraSpaces,
			Trim: trim,
			"Delete extension": deleteExtension,
			"Add extension": addExtension,
			"Replace extension": replaceExtension,
			"Case convention": caseConvention
		};
		//Execution
		var jobName = "";
		for (var i = 1; i < 6; i++) {
			file = actionMethods[s.getPropertyValue("action_" + i)](s, file, i);
			jobName = file.extension ? file.properName + "." + file.extension : file.properName;
			debugLog(s, "RenamePlus, action " + i + " = " + jobName);
		}
		jobName = file.extension ? file.properName + "." + file.extension : file.properName;
		if (!jobName) {
			job.fail("Output name can't be empty");
		}
		job.sendToSingle(job.getPath(), jobName);
	}
}
