const options = {
	removeDiacritics: false,
	removeForbiddenWindowsFileNameCharacters: true,
	removeForbiddenUnixFileNameCharacters: true,
	format: "PascalCase",
	removeExtraSpaces: true,
	trim: false
};

function addSuffix(str, suffix) {
	return str + suffix;
}

function addPrefix(str, prefix) {
	return prefix + str;
}

function parseSegment (segment, str) {
	const matches = segment.match(/([0-9]+)-([0-9]*)|([0-9]*)-([0-9]+)/);
	if (matches && matches[1] !== "0" && matches[2] !== "0") {
		segment = {
			start : !matches[1] ? 0 : parseInt(matches[1]) - 1,
			end : !matches[2] ? str.length : parseInt(matches[2]) - 1
		};
		if (segment.end > str.length){
			segment.end = str.length;
		}
		return segment.end >= segment.start ? segment : null;
	} else {
		return null;
	}
}

function deleteSegment(str, segment) {
	segment = parseSegment(segment, str);
	if (segment === null) {
		return str;
	}
	var newStr = "";
	for (var i = 0; i < str.length; i++) {
		if (i < segment.start || i > segment.end) {
			newStr += str[i];
		}
	}
	return newStr;
}

function keepSegment(str, segment) {
    segment = parseSegment(segment, str);
    if (segment === null) {
        return str;
    }
    var newStr = "";
    for (var i = 0; i < str.length; i++) {
        if (i >= segment.start && i <= segment.end) {
            newStr += str[i];
        }
    }
    return newStr;
}

function replaceBy(str) {
	return str;
}

function searchAndReplace(str, search, replace, repeatType) {
	search = search.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
	const flag = repeatType !== "Once" ? "g" : "";
	if (repeatType !== "Recursive") {
		return str.replace(new RegExp(search, flag), replace);
	}
	var i = 0;
	while (i < 99) {
		var oldStr = str;
		str = str.replace(new RegExp(search, flag), replace);
		if (str === oldStr) {
			return str
		}
		i++;
	}
	return str;
}

function deleteExtension(str) {
    return str.replace(/\..*$/, "");
}

function replaceExtension(str, extension) {
    const match = str.match(/\..*$/);
    return match ? str.replace(/\..*$/, "." + extension) : str + "." + extension;
}

function addExtension(str, extension) {
    return str + "." + extension;
}

function toPascalCase(str) {
	const words = findWords(str);
	for (let i = 0; i < words.length; i++) {
		words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
	}
	return words.join("");
}

function toCamelCase(str) {
	const words = findWords(str);
	for (let i = 0; i < words.length; i++) {
		if (i !== 0) {
			words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
		}
	}
	return words.join("");
}

function toSnakeCase(str) {
	const words = findWords(str);
	return words.join("_").toLowerCase();
}

function findWords(str) {
	str = removeDiacritics(str);
	return str ? str.match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g) : [];
}

function toKebabCase(str) {
	const words = findWords(str);
	return words.join("-").toLowerCase();
}

function toLowerCase(str) {
	return str.toLowerCase();
}

function toUpperCase(str) {
	return str.toUpperCase();
}

function format(str) {
	str = trim(str);
	const formatFunctions = {
		"UPPERCASE": toUpperCase,
		"lowercase": toLowerCase,
		"kebab-case": toKebabCase,
		"snake_case": toSnakeCase,
		"camelCase": toCamelCase,
		"PascalCase": toPascalCase,
	};
	str = formatFunctions[options.format] ? formatFunctions[options.format](str) : str;
	if (extension) {
		str += extension;
	}
	str = trim(str);
	return str;
}

function trim(str) {
	return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
}

function removeExtraSpaces(str) {
	return str.replace(/\s\s+/g, ' ');
}

function removeForbiddenUnixFileNameCharacters(str) {
	return str.replace(/[\/\\]/g, "");
}

function removeForbiddenWindowsFileNameCharacters(str) {
	return str.replace(/[<>:"\/\\|?*]/g, "");
}

function removeDiacritics(str) {
	const letterAccents = {
		a: "[áăắặằẳẵǎâấậầẩẫäạàảāąåǻãɑɐɒ]",
		ae: "[æǽ]",
		b: "[ḅɓ]",
		c: "[ćčçĉɕċ]",
		d: "[ďḓḍɗḏđɖʤǳʣʥǆð]",
		e: "[éĕêěếệềểễëėẹèẻēęẽʒǯʓɘɜɝəɚʚɞ]",
		f: "[ƒſʩﬁﬂʃʆʅɟʄ]",
		g: "[ǵğǧģĝġɠḡɡɣ]",
		h: "[ḫĥḥɦẖħɧɥʮʯų]",
		i: "[íĭǐîïịìỉīįɨĩɩıĳɟ]",
		j: "[ǰĵʝȷɟʄ]",
		k: "[ķḳƙḵĸʞ]",
		l: "[ĺƚɬľļḽḷḹḻŀɫɭłƛɮǉʪʫ]",
		m: "[ḿṁṃɱɯɰ]",
		n: "[ŉńňņṋṅṇǹɲṉɳñǌŋŊ]",
		o: "[óŏǒôốộồổỗöọőòỏơớợờởỡōǫøǿõɛɔɵʘ]",
		oe: "[œ]",
		p: "[ɸþ]",
		q: "[ʠ]",
		r: "[ŕřŗṙṛṝɾṟɼɽɿɹɻɺ]",
		s: "[śšşŝșṡṣʂſʃʆʅ]",
		ss: "[ß]",
		t: "[ťţṱțẗṭṯʈŧʨʧþðʦʇ]",
		u: "[ʉúŭǔûüǘǚǜǖụűùủưứựừửữūųůũʊ]",
		v: "[ʋʌ]",
		w: "[ẃŵẅẁʍʬ]",
		y: "[ýŷÿẏỵỳƴỷȳỹʎ]",
		z: "[źžʑżẓẕʐƶ]",
	};
	for (const letter in letterAccents) {
		str = str.replace(new RegExp(letterAccents[letter], "g"), letter);
		str = str.replace(new RegExp(letterAccents[letter].toUpperCase(), "g"), letter.toUpperCase());
	}
	return str;
}

function main() {
	let str = process.argv[2];
	let extension = process.argv[3];
    str = deleteExtension(str);

	console.log(`"${str}"`);
}

main();